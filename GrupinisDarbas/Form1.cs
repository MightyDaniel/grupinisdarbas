﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrupinisDarbas
{
    public enum Veiksmas
    {
        Suma = 1,
        Atimtis = 2,
        Daugyba = 3,
        Dalyba = 4
    }

    public partial class Form1 : Form
    {
        public double Skaicius1 { get; set; }

        public double Skaicius2 { get; set; }

        public Veiksmas Veiksmas { get; set; }
        
        public Form1()
        {
            InitializeComponent();
          

        }

        public double Sumuok()
        {
            return Skaicius1 + Skaicius2;
        }

        public double Atimk()
        {
            return Skaicius1 - Skaicius2;
        }
        public double Daugink()
        {
            return Skaicius1 * Skaicius2;
        }
        public double Dalink()
        {
            return Skaicius1 / Skaicius2;
        }

        public void AtlikVeiksmą()
        {
            Skaicius1 = double.Parse(textBox1.Text);
            Skaicius2 = double.Parse(textBox2.Text);
            switch(Veiksmas)
            {
                case Veiksmas.Suma:
                    {
                        textBox3.Text = Sumuok().ToString();
                    };
                    break;
                case Veiksmas.Atimtis:
                    {
                        textBox3.Text = Atimk().ToString();
                    };
                    break;
                case Veiksmas.Daugyba:
                    {
                        textBox3.Text = Daugink().ToString();
                    };
                    break;
                case Veiksmas.Dalyba:
                    {
                        textBox3.Text = Dalink().ToString();
                    };
                    break;
                default:
                    break;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Veiksmas = Veiksmas.Suma;
            ClearLabel.Text = "+";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AtlikVeiksmą();
            
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Veiksmas = Veiksmas.Atimtis;
            ClearLabel.Text = "-";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Veiksmas = Veiksmas.Daugyba;
            ClearLabel.Text = "*";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Veiksmas = Veiksmas.Dalyba;
            ClearLabel.Text = "/";
        }

        private void ClearLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
